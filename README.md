# osrm

## Introduction

**Important: This is not a main repository for the project 23CS043. Please refer to https://gitlab.com/cityu-cs-fyp/Codebase for the codebase I uploaded to the FYPMS.**

This is a seperate sub-repository for the project "A mobile-based logistic management system (23CS043)". This repository contains the map infrastructure for the project. 

## Pre-requisites
- Docker (preferably >= 24.0.0).
- GCC/Clang.
- Zip.
- Internet connection (check firewall whitelist setting if external request is needed).

## Standalone deployment

Since the map files exceed the Github/Gitlab free upload limit, the author uploaded them using split zip.

To piece the split files together and deploy it on Docker, one must follow the following procedures.

1. Clone and decompress this repository
2. CD into the decompressed folder.
3. Start assembling using `zip -s0 small_osrm.zip --out osrm.zip`. This will create an `osrm.zip` from `small_osrm.zip`, `small_osrm.z01`, and `small_osrm.z02`.
4. Decompress `osrm.zip`.
5. CD into the decompressed `osrm` folder.
6. Run `hosting.sh`.

## Integration with the mobile-based logistic management system project

- Perform the steps in "Standalone deployment" until step 4.
- Move the `osrm` folder to the mobile-based logistic management system project's repository folder.
- Run `docker-compose build && docker-compose up`.